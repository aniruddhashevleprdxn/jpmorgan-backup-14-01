(function() {

      // if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      //   $(button).on('touchstart', function(e) {
      //     e.preventDefault();
      //     $(this).trigger('click');
      //   });
      // }


  var header = (function() {
    // /////////
    // Cache Dom
    // /////////
    var hamIcon = '.hamburger';
    var mainNav = 'header .main-nav';
    var headerLink = '.main-nav li a';

    /**
     * Functions that are gonna be used for th header ofthe site.
     */

    // Open menu on click
    function hamMenu() {

      if ($(this).hasClass('open')) {
        $(this).removeClass('open');
        $(mainNav).animate({
          opacity: 0,
          visibility: 'hidden'
        }, 200);
      } else {
        $(this).addClass('open');
        $(mainNav).animate({
          opacity: 1,
          visibility: 'visible'
        }, 200);
      }
    }

    // Add class open on hover
    function hoverOver() {
      $(this).addClass('open');
    }

    // Remove class open on hover
    function hoverOut() {
      $(this).removeClass('open');
    }

    // Bind Event
    if ($(window).width() > 1024) {
      $(hamIcon).hover(hoverOver, hoverOut);
      $(mainNav).css('width', $(window).width() + 'px');
    }

    if ($(window).width() < 1025) {
      $(hamIcon).on('click', hamMenu);

      if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
        $(hamIcon).on('touchstart', function(e) {
          e = window.event || e;

          if(this === e.target) {
            e.preventDefault();
            $(this).trigger('click');
          }

        });
      }
      $(mainNav).css('width', $(window).width() + 'px');
    }

    $(window).resize(function(event) {
      /* Act on the event */

      $(mainNav).css('width', $(window).width() + 'px');
      $('.video-container iframe').css('width', '100%');
    });

    // Highlight current page
    $(headerLink).each(function(index, el) {
      if ($(this).attr('href') === (window.location.pathname).replace('/', '')) {
        $(this).addClass('current-page');
      } else if (window.location.pathname === '/') {
        if ($(this).attr('href') === 'index.html') {
          $(this).addClass('current-page');
        }
      }
    });
  })();

  var homePage = (function() {
    // ///////
  	// cacheDom
    // ///////
  	var $seemoreArrow = $(".down-arrow");
  	var $back_to_top = $('.cd-top');
  	var scroll_top_duration = 700;
  	var offset_opacity = 1200;
  	var offset = 300;
  	var bannerHeight = ($(window).height() - 76);
    var videoBoxPlay = '.openLightBox';
    var overlay = '.contentBox , .overlay ,.close-button-home';
    var overlay2 = '.videoContent , .overlay2 ,.close-button-home';
    var closeIt = '.overlay2,.close-button-home';

      $('.banner').css('height' , bannerHeight);
      if($(window).width() < 767 && $(window).width() > 420) {
        bannerHeight = bannerHeight + 26;
        $('.banner').css('height' , bannerHeight);
      }
    /**
     * Home Page functions
     */

    // This function is used for the bounce effect
    function seeMorebounce(element) {
      setTimeout(function() {
        element.stop().animate({
          'bottom': "+=10" + 'px'
        }, 300, function() {
          element.stop().animate({
            'bottom': "-=10" + 'px'
          }, 300, function() {
            element.stop().animate({
              'bottom': "+=5" + 'px'
            }, 100, function() {
              element.stop().animate({
                'bottom': "-=5" + 'px'
              }, 100, function() {
                seeMorebounce(element);
              });
            });
          });
        });
      }, 2000);
    }

    // This function will be used to make our particular element bounce
    function moreBounce() {
      $seemoreArrow.each(function() {
        seeMorebounce($(this));
      });
    }

    function getGridSize() {
      return (window.innerWidth < 330) ? 1 :
             (window.innerWidth < 570) ? 2 :
             (window.innerWidth <= 768) ? 3 :
             (window.innerWidth > 768) ? 4 : 4
    }


    $('#carousel').flexslider({
      animation: "slide",
      controlNav: true,
      animationLoop: false,
      slideshow: false,
      itemWidth: 210,
      maxItems : getGridSize(),
      minItems : getGridSize(),
      asNavFor: '#slider',
      start: function (slider) {
        flexslider = slider;
      }
    });


    // Slider
    $('#slider').flexslider({
      animation: "slide",
      // useCSS: false,
      controlNav: true,
      animationLoop: true,
      slideshow: false,
      sync: "#carousel"
    });


    $(window).resize(function() {
      var gridSize = getGridSize();

      flexslider.vars.minItems = gridSize;
      flexslider.vars.maxItems = gridSize;
    });

    // Flexslider functionality for making video play
    var customFlexSlide = (function() {
      //cache Dom
      // $('li').removeClass('flex-active-slide');
      var button = '#carousel li';
      var imagePlay = '#slider .image';

//Using the GBWM Wealth Projection System
    function playerSlide1() {
     $('#first .iframe-wrapper-slide1').show();
     $("#first .iframe-wrapper-slide1").brightcoveVideo({
       "playerID": "899306521001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302001",
       "@videoPlayer": 4699077573001,
       "templateReadyHandler": onTemplateReady1,
       "autoStart" : true
     });
   }

    function onTemplateReady1() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
        $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }

    function playerSlide2() {
     $('.iframe-wrapper-slide2').show();
     $(".iframe-wrapper-slide2").brightcoveVideo({
       "playerID": "899306521001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302001",
       "@videoPlayer": 4686156566001,
       "templateReadyHandler": onTemplateReady2,
       "autoStart" : true
     });
   }

    function onTemplateReady2() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }

    function playerSlide3() {
     $('.iframe-wrapper-slide3').show();
     $(".iframe-wrapper-slide3").brightcoveVideo({
       "playerID": "899306521001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302001",
       "@videoPlayer": 4686205415001,
       "templateReadyHandler": onTemplateReady3,
       "autoStart" : true
     });
   }

    function onTemplateReady3() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }

    function playerSlide4() {
     $('.iframe-wrapper-slide4').show();
     $(".iframe-wrapper-slide4").brightcoveVideo({
       "playerID": "899306521001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302001",
       "@videoPlayer": 4686229230001,
       "templateReadyHandler": onTemplateReady4,
       "autoStart" : true
     });
   }

    function onTemplateReady4() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }
    function playerSlide5() {
     $('.iframe-wrapper-slide5').show();
     $(".iframe-wrapper-slide5").brightcoveVideo({
       "playerID": "899306521001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302001",
       "@videoPlayer": 4686156572001,
       "templateReadyHandler": onTemplateReady5,
       "autoStart" : true
     });
   }

    function onTemplateReady5() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }

    function playerSlide6() {
     $('.iframe-wrapper-slide6').show();
     $(".iframe-wrapper-slide6").brightcoveVideo({
       "playerID": "899306521001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302001",
       "@videoPlayer": 4686229132001,
       "templateReadyHandler": onTemplateReady6,
       "autoStart" : true
     });
   }

    function onTemplateReady6() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }

    function playerSlide7() {
     $('#last .iframe-wrapper-slide7').show();
     $("#last .iframe-wrapper-slide7").brightcoveVideo({
       "playerID": "899306521001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302001",
       "@videoPlayer": 4686229130001,
       "templateReadyHandler": onTemplateReady7,
       "autoStart" : true
     });
   }

    function onTemplateReady7() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }

    function player8() {
     $('.video-container8').show();
     $(".videoContent .video-container8").brightcoveVideo({
       "playerID": "899306521001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302001",
       "@videoPlayer": 4686205415001,
       "templateReadyHandler": onTemplateReady8,
       "autoStart" : true
     });
   }

    function onTemplateReady8() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }

    function player9() {
     $('.video-container9').show();
     $(".videoContent .video-container9").brightcoveVideo({
       "playerID": "899306521001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302001",
       "@videoPlayer": 4699077573001,
       "templateReadyHandler": onTemplateReady9,
       "autoStart" : true
     });
   }

    function onTemplateReady9() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }


   function lightBoxPlay(e) {
    e.preventDefault();
    $(overlay2).fadeIn(600);
    var dataSrc = $(this).attr('data-lb');
    if(dataSrc == '1') {
      $('.video-container').hide();
      player9();
    }
    else if(dataSrc == '2') {
      $('.video-container').hide();
      player8();
    }
   }

      function lightBoxStop(e) {
        e.preventDefault();
        $(overlay2).fadeOut(400);
        $('html , body').css('overflow','auto');
      }

   $(videoBoxPlay).on('click touchstart', lightBoxPlay);
   $(closeIt).on('click touchstart',lightBoxStop );

    window.addEventListener("orientationchange", function() {
       var width = $('.iframe-wrapper-slide').width();
       var height = $('.iframe-wrapper-slide').outerHeight();
       $('.mobileExperience').data("brightcoveVideo").experience.setSize(width, height);
     }, false);


      if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
        $(button).on('touchstart', function(e) {
          e.preventDefault();
          $(this).trigger('click');
        });
      }


      // function for adding video on image click
      function imagePlayFunc() {
        $(this).remove();
        $('#slider .slides li:nth-of-type(2)').addClass('flex-active-slide');
        $('#carousel .slides li:nth-of-type(1)').addClass('flex-active-slide');
        playerSlide1();
      }


      // function for adding video
      function upVideo(e) {
      e.preventDefault();
      e.stopPropagation();
      var active = $(this).hasClass('flex-active-slide');
      var index = $(this).index();
        if(active) {
          if(index == 0) {
            playerSlide1();
          }
          else if(index == 1){
            playerSlide2();
          }
          else if(index == 2){
            playerSlide3();
          }
          else if(index == 3){
             playerSlide4();
          }
          else if(index == 4){
            playerSlide5();
          }
          else if(index == 5){
            playerSlide6();
          }
          else if(index == 6){
            playerSlide7();
          }
          $('.image').css('opacity' , 0);
          $('ul').css('margin-left',0);
          $('#slider .slides .flex-active-slide').children('span').css('display','none');
        }
      }

      // bind events
      $(button).on('touchend click ',upVideo);
      $(imagePlay).on('touchend click', imagePlayFunc);
    })();

    var homeLightBox = (function(){
      // Cache Dom
      var lightBoxHome = '.lightboxHome';
      var overlay = '.contentBox , .light-box-content, .overlay ,.close-button-home';
      var closeIt = '.overlay, .close-button-home';

      // function for opening lightBox
      function lightBoxHomeOpen(e){
        e.preventDefault();
        $(overlay).fadeIn(600);
        $('html , body').css('overflow','hidden');
      }

      // Function for closing home page lightBoxHome
      function lightBoxHomeClose(e) {
        e.preventDefault();
        $(overlay).fadeOut(400);
        $('html , body').css('overflow','auto');
      }

      // Bind Event
      $(lightBoxHome).on('click',lightBoxHomeOpen);
      $(closeIt).on('click',lightBoxHomeClose);
    })();

    //hide or show the "back to top" link
    $(window).scroll(function(){
    	if( $(this).scrollTop() > offset ) {
    		$back_to_top.addClass('cd-is-visible');
    	} else {
    		$back_to_top.removeClass('cd-is-visible cd-fade-out');
    	}

    	if( $(this).scrollTop() > offset_opacity ) {
    		$back_to_top.addClass('cd-fade-out');
    	}
    });

    //smooth scroll to top
    $back_to_top.on('click', function(event){
    	event.preventDefault();

    	$('body,html').animate({
    		scrollTop: 0 ,
    	 	}, scroll_top_duration
    	);
    });

    // Scroll to a section of the page.
    function scrollToSection(element, duration) {

      $('html, body').animate({
        scrollTop: $(element).offset().top
      }, duration);
    }

    // //////////////
    // Binding events
    // //////////////
    $('.scroll-to').on('click', function(e) {
    	e.preventDefault();

    	scrollToSection('#slider', 1000);
    });

    $('.how').on('click', function(e) {
      e.preventDefault();

      scrollToSection('#howWork', 1000);
    });

    // Add height for the banner as per the viewport
    $(window).resize(function(event) {
      var flexHeight = $('#slider .flex-active-slide').outerHeight();
      $('#slider .flex-viewport').height(flexHeight);
      $('.banner').css('height' , ($(window).height() - 76) + 'px');
      var bannerHt = $('.banner').height();
      if($(window).width() < 767 && $(window).width() > 420) {
        $('.banner').css('height' , bannerHt+26);
      }
    });

    moreBounce();
  })();

  var thoughtLeadSec = (function() {
    //Cache Dom
    var authorName;
    var hfifthFont;
    var mainPageHead;
    var fontSize = '#fontSize';
    var increaseFont = '.increaseFont';
    var decreaseFont = '.decreaseFont';
    var initialHfithFont = parseInt($('.heading-resize').css('font-size'));
    var initialMainPageHead = parseInt($('.securityHeading').css('font-size'));
    var lightBox = '.lightbox';
    var closeLightBox = '.overlay,.imageBox';

    // for Apple device touch

    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      $(fontSize,increaseFont,decreaseFont ).on('touchstart', function(e) {
        e.preventDefault();
        $(this).trigger('click');
      });
    }

    // for Changing the font size.
    function fontSizeMenu(e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).siblings('.fontHover').slideToggle(400);
    }

    // Font size menu close

    function fontSizeMenuClose(e){
      e.stopPropagation();
      $('.fontHover').slideUp(400);
    }

    // function for incresing size
    function increaseFontSize(e) {
      e.stopPropagation();
      e.preventDefault();
      hfifthFont = parseInt($('.heading-resize').css('font-size'));
      mainPageHead = parseInt($('.securityHeading').css('font-size'));
      federalRules = parseInt($('.thought-security .main-content .federal-rules').css('font-size'));
      if (hfifthFont < initialHfithFont + 6 && mainPageHead < initialMainPageHead + 6) {
        $('.heading-resize, .text-resize').css('font-size', hfifthFont + 2);
        $('.securityHeading').css('font-size', mainPageHead + 2);
        $('.thought-security .main-content .federal-rules').css('font-size', federalRules + 2);
      }
    }

    // Function for decreasing size.
    function decreaseFontSize(e) {
      e.stopPropagation();
      e.preventDefault();
      hfifthFont = parseInt($('.heading-resize').css('font-size'));
      mainPageHead = parseInt($('.securityHeading').css('font-size'));
      federalRules = parseInt($('.thought-security .main-content .federal-rules').css('font-size'));
      if (hfifthFont > initialHfithFont && mainPageHead > initialMainPageHead) {
        $('.heading-resize, .text-resize').css('font-size', hfifthFont - 2);
        $('.securityHeading').css('font-size', mainPageHead - 2);
        $('.thought-security .main-content .federal-rules').css('font-size', federalRules - 2);
      }
    }

    // Function for opening lightbox
    function lightBoxOpen(e) {
      e.preventDefault();
      var source = $(this).attr('href');
      $(closeLightBox).fadeIn(600);
      $('.imageBox').append('<img src="' + source + '" alt= "graph" />');
    }

    // Function for closing lightBox
    function lightBoxClose(e) {
      e.preventDefault();
      $(closeLightBox).fadeOut(400);
      $('.imageBox > img').remove();
    }

    // Bind Event
    $(lightBox).on('click touchstart', lightBoxOpen);

    $(closeLightBox).on('click touchstart', lightBoxClose);

    $(fontSize).on('click', fontSizeMenu);

    $('body').on('click' , fontSizeMenuClose);

    $(increaseFont).on('click', increaseFontSize);

    $(decreaseFont).on('click', decreaseFontSize);
  })();

  var commonQuestion = (function() {

    // cache Dom
    var quesGroup = '.groups > span';
    var subQuestions = '.subQuestions  .question';

    //Main question group
    function listClick(e) {
      var hasClassView = $(this).hasClass('view-more');
      e.stopPropagation();
      e.preventDefault();
      $('.answer').slideUp(700);
      if ($(this).siblings('ul').is(':visible')) {
        $(this).siblings('ul').slideUp(700);
        $(this).siblings('.view-more').removeClass('selected-second-image').attr('title', 'View All Questions');
        if(hasClassView) {
          $(this).removeClass('selected-second-image').attr('title', 'Hide');
        }
      } else {
        $(quesGroup).siblings('ul').slideUp(700);
        $(quesGroup).siblings('.view-more').removeClass('selected-second-image').attr('title', 'View All Questions');
        $(this).siblings('ul').slideDown(700);
        $(this).siblings('.view-more').addClass('selected-second-image').attr('title', 'Hide');
        if(hasClassView) {
        $(this).addClass('selected-second-image').attr('title', 'Hide');
        }
      }
    }

    // for sub-question dropdown

    function innerListClick(e) {
      e.stopPropagation();
      e.preventDefault();
      if ($(this).siblings('.answer').is(':visible')) {
        $(this).siblings('.answer').slideUp(700);
        $(this).find('img').attr('src', 'images/cq.small-plus.png');
        $(this).find('.showMore').attr('title', 'Open');
      } else {
        $(this).siblings('.answer').slideDown(700);
        $(this).find('img').attr('src', 'images/cq.small-minus.png');
        $(this).find('.showMore').attr('title', 'Hide');
      }
    }

    function stopPropogation(e) {
      e.preventDefault();
    }

    // Bind Event

    $(quesGroup).on('click', listClick);
    $(subQuestions).on('click', innerListClick);
  })();

  // for email functionality..

  var sendMail = (function() {
    //cache Dom
    var securityLink = 'mailto:test@example.com?subject=When Should You Start Taking Social Security?&body=Dear [Client],%0A%0AI am pleased to share with you a new publication from the J.P. Morgan Advice Lab entitled: When should you start taking Social Security? Starting at age 62, you have the opportunity to begin receiving Social Security benefits. Deciding when to begin receiving benefits is a complicated question, especially since maximum Social Security payments this year would be roughly equal to the income from a US Treasury portfolio worth more than $1 million. A good rule of thumb is that Social Security is insurance against having no money in your old age, and you must determine whether you want to maximize the protection offered by Social Security and whether you can afford to do so.%0A%0AMaking these decisions involves understanding how your benefits are calculated, the financial impact of benefits taken at different ages, and your wealth as well as income needs in retirement.%0A%0AI would be happy to work with you and your advisors to further explore how we can help you understand your options, and how different choices may fit into your overall retirement plan.%0A%0ASincerely,%0A[Name]%0A%0AJPMorgan Chase %26 Co. and its subsidiaries do not render accounting, legal or tax advice.  Estate planning requires legal assistance. Clients should consult with their independent advisors concerning such matters.';

    var taxLink = 'mailto:test@example.com?subject=Should You Put More into Your Tax-Deferred Retirement Accounts?&body=Dear [Client],%0A%0AI am pleased to share with you a new publication from the J.P. Morgan Advice Lab entitled: Should you put more into your tax-deferred retirement accounts? %0A%0AStarting at age 50, you have the opportunity to boost the value of the assets in your retirement accounts. But, before you decide how much to put in a 401(k) or IRA, maximizing the benefits of the catch-up contributions requires an understanding of how much you already have saved and what your future income may be.%0A%0AI would be happy to work with you and your advisors to further explore how we can help you understand your options, and how different choices may fit into your overall retirement plan.%0A%0ASincerely,%0A[Name]%0A%0AJPMorgan Chase %26 Co. and its subsidiaries do not render accounting, legal or tax advice.  Estate planning requires legal assistance. Clients should consult with their independent advisors concerning such matters.';

    var jobsLink = 'mailto:test@example.com?subject=Retiring or Changing Jobs?&body=Dear [Client],%0A%0AI am pleased to share with you two new publications from the J.P. Morgan Advice Lab that outline some key questions and considerations to focus on prior to rolling over your employer sponsored plan to an IRA.%0A%0AThis new publication entitled, “Retiring or changing jobs?”  focuses on important factors concerning the assets in your 401(k), 403(b) or other employer-sponsored retirement plan which may be affected when you retire or change jobs. I have also included a quick guide “Rollover to an IRA? To a new plan?” to help you identify the option that may make the most sense for your needs.%0A%0AAnswering the question: “Should you withdraw your funds?” or “Should you keep the fund in a tax-deferred account?” can have significant financial implications. It is important to understand options available, so that you can make the decision that best suits your needs.%0AI would be happy to work with you and your advisors to determine how these factors may impact your situation, and help identify the appropriate strategies for your circumstances and goals.%0A%0ASincerely,%0A[Name]%0A%0AJPMorgan Chase %26 Co., its affiliates and employees do not provide tax, legal or accounting advice. This material has been prepared for informational purposes only. You should consult your own tax, legal and accounting advisors before engaging in any financial transactions.%0A%0A© 2015 JPMorgan Chase %26 Co. All rights reserved.';


    var securityMail = '.security-mail';
    var jobsMail = '.jobs-mail';
    var taxMail = '.tax-mail';


    // Function for sending mail
    function sendMailToSec(e) {
      $(this).attr('href', securityLink);
    }
    function sendMailToJob(e) {
      $(this).attr('href', jobsLink);
    }
    function sendMailToTax(e) {
      $(this).attr('href', taxLink);
    }


    // Bind Events

    $(securityMail).on('click',sendMailToSec);
    $(jobsMail).on('click',sendMailToJob);
    $(taxMail).on('click',sendMailToTax);

  })();


  var additionalResource = (function(){
    // Cache Dom

    var videoBox = '.videoBox';
    var overlay = '.contentBox , .overlay ,.close-button-home';

    //  function for lightbox additional resource
    function lightBoxVideo(e) {
      var data = $(this).attr('src-data');
      $('.video-container').hide();
      if(data == 'a') {
          player1();
      }
      else if(data == 'b') {
        player2();
      }
      else if(data == 'c') {
        player3();
      }
      else if(data == 'd') {
        player4();
      }
      else if(data == 'e') {
        player5();
      }
      else if(data == 'f') {
        player6();
      }
      else if(data == 'g') {
        player7();
      }
      e.preventDefault();
      $(overlay).fadeIn(600);
    }

//Using the GBWM Wealth Projection System
    function player1() {
     $('.video-container1').show();
     $(".contentBox .video-container1").brightcoveVideo({
       "playerID": "899306521001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302001",
       "@videoPlayer": 4699077573001,
       "templateReadyHandler": onTemplateReady11,
       "autoStart" : true
     });
   }

    function onTemplateReady11() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }


//What is Goals-Based Wealth Management?
  function player2() {
    $('.video-container2').show();
    $('.contentBox .video-container2').brightcoveVideo({
      "playerID": "4686156572001",
      "htmlFallback": "true",
      "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
      "experienceID": "myExperience1939911302002",
      "@videoPlayer": 4686156566001,
      "templateReadyHandler": onTemplateReady2,
      "autoStart" : true
    });
  }

  function onTemplateReady2() {
    var $player = $(this);
    window.addEventListener("resize", function() {
      var width = $player.width();
      var height = $player.outerHeight();
      $player.data("brightcoveVideo").experience.setSize(width, height);
    }, false);
  }

// What is a Wealth StrategistSM?

  function player3() {
    $('.video-container3').show();
    $(".contentBox .video-container3").brightcoveVideo({
      "playerID": "4686156572001",
      "htmlFallback": "true",
      "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
      "experienceID": "myExperience1939911302003",
      "@videoPlayer": 4686205415001,
      "templateReadyHandler": onTemplateReady3,
      "autoStart" : true
    });
  }

  function onTemplateReady3() {
    var $player = $(this);
    window.addEventListener("resize", function() {
      var width = $player.width();
      var height = $player.outerHeight();
      $player.data("brightcoveVideo").experience.setSize(width, height);
    }, false);
  }

//How does the Wealth Strategist work with the Integrated Team?
  function player4() {
    $('.video-container4').show();
    $(".contentBox .video-container4").brightcoveVideo({
      "playerID": "4686156572001",
      "htmlFallback": "true",
      "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
      "experienceID": "myExperience1939911302004",
      "@videoPlayer": 4686229230001,
      "templateReadyHandler": onTemplateReady4,
      "autoStart" : true
    });
   }

  function onTemplateReady4() {
    var $player = $(this);
    window.addEventListener("resize", function() {
      var width = $player.width();
      var height = $player.outerHeight();
      $player.data("brightcoveVideo").experience.setSize(width, height);
    }, false);
  }
//When should I involve a Wealth Strategist?

    function player5() {
     $('.video-container5').show();
     $(".contentBox .video-container5").brightcoveVideo({
       "playerID": "4686156572001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302005",
       "@videoPlayer": 4686156572001,
       "templateReadyHandler": onTemplateReady5,
       "autoStart" : true
     });
   }

    function onTemplateReady5() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }



//What is the “Core/Surplus” approach?
    function player6() {
     $('.video-container6').show();
     $(".contentBox .video-container6").brightcoveVideo({
       "playerID": "4686156572001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302006",
       "@videoPlayer": 4686229132001,
       "templateReadyHandler": onTemplateReady6,
       "autoStart" : true
     });
   }



    function onTemplateReady6() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }


//Peak to Trough Analysis
    function player7() {
     $('.video-container7').show();
     $(".contentBox .video-container7").brightcoveVideo({
       "playerID": "4686156572001",
       "htmlFallback": "true",
       "playerKey": "AQ~~,AAAAzzLoQME~,DyZ0aPNtMWEAbuJbrgxr0Zz99nBCLt0y",
       "experienceID": "myExperience1939911302007",
       "@videoPlayer": 4686229130001,
       "templateReadyHandler": onTemplateReady7,
       "autoStart" : true
     });
   }



    function onTemplateReady7() {
     var $player = $(this);
     window.addEventListener("resize", function() {
       var width = $player.width();
       var height = $player.outerHeight();
       $player.data("brightcoveVideo").experience.setSize(width, height);
     }, false);
    }



// resize for brightCove videos in additional resources

    window.addEventListener("orientationchange", function() {
       var width = $('.video-container').width();
       var height = $('.video-container').outerHeight();
       $('.mobileExperience').data("brightcoveVideo").experience.setSize(width, height);
     }, false);



     //  function for closing lightbox additional resource
    function lightBoxVideoClose(e) {
      $('.video-container').hide();
      e.preventDefault();
      $(overlay).fadeOut(600);
    }


    // Bind Event
    $(videoBox).on('click touchstart',lightBoxVideo);
    $('.overlay , .close-button-home').on('click touchstart', lightBoxVideoClose);
  })();
})();
